Este ejercicio se basa en la creación de una App de reservas

    1-. En primer lugar hemos de crear las clases necesarias para trabajar con la arquitectura VMMV. Estas clases son: 
        a) La clase DAO, que interacciona directamente con la base de datos, ya que es la única clase, realmente una interfaz,
        que está conectada con la base de datos.
        b) La clase Database, en la que enlazas la clase modelo para crear una tabla, es decir, por cada clase modelo, en este caso
        Booking, además de la version y obtener la instancia de la propia base de datos.
        c) La clase Repositorio, que interactúa directamente con el DAO, y contiene todos los métodos de insercion, borrado, actualizar,
        etc.
        d) Las clases ViewModel, normalmente una para actividad, aunque yo he utilizado para la insercion y para el Activity principal el mismo
        ViewModel, esta clase hace una instancia del repositorio y le manda la acción que se le ha pedido con un dato tipo LiveData.
    
    2.- Una vez hechas estas clases, solo hemos de crear varias entradas en la base de datos para que cuando se inicie por primera vez
    no esté vacía y se pueda comprobar el funcionamiento.

    3.- Cuando ya tenemos estas entradas, creamos el RecyclerView y el Adapter y mediante el metodo "observe" de la clase LiveData obtenemos
    la lista de entradas y la instanciamos en el adapter.
